const multiply = require("./multiply");
const second = 1000;
const minute = second * 60;
const hour = minute * 60;
const day = hour * 24;

test("multiply second * minutes to equal ", () => {
  expect(multiply(second, minute)).toBe(60000000);
});

it("will check the values and pass", () => {
  const count = {
    countdown: new Date("2022-08-20"),
  };

  expect(count).toMatchSnapshot({
    countdown: expect.any(Date),
  });
});
