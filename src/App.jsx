import React from "react";
import CountdownForm from "./components/CountdownForm.jsx";
import "./App.css";
import MyVideo from "./assets/IphoneVideo.mp4";
import styled from "styled-components";

const VideoBackground = styled.video`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;
const OverlayDiv = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.2);
`;
export default function App() {
  return (
    <div className="App">
      <VideoBackground src={MyVideo} loop muted autoPlay></VideoBackground>
      <OverlayDiv>
        <CountdownForm />
      </OverlayDiv>
    </div>
  );
}
