import React, { useEffect, useState } from "react";
import "../countDown.css";
import styled from "styled-components";

const Container = styled.div`
  width: 880px;
  height: 400px;
  color: black;
  margin: 300px auto;
  padding: 2 5px 50px;
  border-radius: 10px;
  z-index: 2;
  background: rgba(255, 255, 255, 0.5);
  display: flex;
  justify-content: center;
`;
const H1 = styled.h1`
  font-size: 50px;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 20px;
  color: #006959;
`;
const List = styled.li`
  display: inline-block;
  font-size: 40px;
  list-style-type: none;
  padding: 10px;
  text-transform: uppercase;
  color: #006959;
`;
const Span = styled.span`
  display: block;
  font-size: 80px;
  text-align: center;
  margin-top: 20px;
`;
export default function CountdownForm() {
  const [days, setDays] = useState();
  const [hours, setHours] = useState();
  const [minutes, setMinutes] = useState();
  const [seconds, setSeconds] = useState();
  const [message, setMessage] = useState();

  // const today = new Date().toISOString().split("T");
  // console.log("today", today);

  const second = 1000;
  const minute = second * 60;
  const hour = minute * 60;
  const day = hour * 24;

  useEffect(() => {
    let countdown = new Date("2022-08-20 18:59:27").getTime();
    let interval = setInterval(() => {
      const now = new Date().getTime();
      const difference = countdown - now;

      const days = Math.floor(difference / day);
      const hours = Math.floor((difference % day) / hour);
      const minutes = Math.floor((difference % hour) / minute);
      const seconds = Math.floor((difference % minute) / second);

      setDays(days);
      setHours(hours);
      setMinutes(minutes);
      setSeconds(seconds);

      if (difference < 0) {
        setMessage("De nieuwe iPhoneXX is er!");
        clearInterval(interval);
        setDays(0);
        setHours(0);
        setMinutes(0);
        setSeconds(0);
      }
    }, second);
  }, []);

  return (
    <Container>
      <div>
        <H1>Countdown</H1>
        <H1>{message}</H1>
        <ul>
          <List>
            <Span>{days}</Span>
            {days === 1 ? <List>Dag</List> : <List>Dagen</List>}
          </List>
          <List>
            <Span>{hours}</Span>
            {hours === 1 ? <List>Uur</List> : <List>Uren</List>}
          </List>
          <List>
            <Span>{minutes}</Span>
            {minutes === 1 ? <List>Minute</List> : <List>Minuten</List>}
          </List>
          <List>
            <Span>{seconds}</Span>
            {seconds === 1 ? <List>Second</List> : <List>Seconden</List>}
          </List>
        </ul>
      </div>
    </Container>
  );
}
